
import { useDispatch, useSelector } from 'react-redux';
import { updateInputValue, updateList } from './listSlice';
import { nanoid } from 'nanoid';
const TodoGenerator = ()=>{

    const inputValue = useSelector((state)=>state.list.inputValue)
    const dispatch = useDispatch()
    const getText = (event) =>{
            dispatch(updateInputValue(event.target.value))
        
        
    }
    const addItem = ()=>{
        if(inputValue!==''){
            dispatch(updateList({
                id:nanoid(),
                content:inputValue,
                done:false
            }))
        }
       
    }
    return (
       <div className='todoGenerator'>
       
        <input onChange={getText} value={inputValue} className='input'/>
        <button className="button" onClick={addItem}>
            Add
        </button>
       </div>
    )
}
export default TodoGenerator