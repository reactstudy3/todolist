import { useDispatch } from "react-redux"
import { deleteItem, updateStatus } from "./listSlice"


const TodoItem = (props)=>{
    const {item}=props
    const dispatch = useDispatch()
    const delStatus = ()=>{
        dispatch((updateStatus(item.id)))
    }
    const deleteToDo = (item)=>{
        dispatch(deleteItem(item))
    }
    return (
        <div className="item">
        <div className='todoItem'>
        <label onClick={delStatus}>
            {item.done? <del>{item.content}</del>:item.content}
        </label>
        </div>
        <button onClick={()=>deleteToDo(item)} className="deleteBtn">X</button>
        </div>
        
    )
}
export default TodoItem