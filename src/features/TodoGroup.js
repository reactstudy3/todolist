import TodoItem from "./TodoItem"
import { useSelector } from "react-redux"

const TodoGroup = () => {
    const itemList = useSelector((state)=>state.list.todoList)
    return (
        <div>
            {itemList.map((item) => {
                return (
                    <div className="itemGroup">
                    <TodoItem key={item.id} item={item} />
                    </div>
                    
                );
            })}
        </div>
    )
}

export default TodoGroup